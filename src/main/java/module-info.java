module com.example.ly {
    requires javafx.controls;
    requires javafx.graphics;
    requires javafx.fxml;
    requires java.sql;


    opens com.example.ly to javafx.fxml;
    exports com.example.ly;
}