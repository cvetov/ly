package com.example.ly;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;

public class HelloController {

    private boolean isVisible = false;

    public static int coltry = 0;

    @FXML
    TextField loginField;
    @FXML
    TextField passwordField;

    @FXML
    PasswordField pass;
    @FXML
    RadioButton radio;

    @FXML
    protected void onHelloButtonClick() throws SQLException, IOException, ClassNotFoundException {
        if (coltry < 4) {
            if (DBConnect.getDBConnection(loginField.getText(), passwordField.getText())) {
                login();
            } else {
                coltry++;
            }
        } else {
            toCaptcha();
        }
    }

    private void login() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("first-view.fxml"));
        Stage stage = (Stage) loginField.getScene().getWindow();
        stage.setScene(new Scene(root));
        stage.show();
    }

    private void toCaptcha() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(CaptchaController.class.getResource("captcha-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    public void onVisibleText(){
        pass.setText(passwordField.getText());
    }

    @FXML
    public void onPrivateText(){
        passwordField.setText(pass.getText());
    }

    public void setPassVisible(ActionEvent actionEvent) {
        if(isVisible){
            pass.setText(passwordField.getText());
            passwordField.setVisible(false);
            pass.setVisible(true);
            isVisible = false;
        } else {
            passwordField.setText(pass.getText());
            pass.setVisible(false);
            passwordField.setVisible(true);
            isVisible = true;
        }
    }

    public void onPrivateText(KeyEvent keyEvent) {
        passwordField.setText(pass.getText());
    }
}