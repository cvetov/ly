package com.example.ly;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class Order implements Initializable {

    private ObservableList<DataSale> history = FXCollections.observableArrayList();

    @FXML
    private TableView<DataSale> tableview;


    @FXML
    Label pOrder;

    @FXML
    private ComboBox auto;

    @FXML
    private ComboBox client;

    @FXML
    private TextField date;

    @FXML
    private ComboBox form;

    @FXML
    private TableColumn<DataSale, String> column1;

    @FXML
    private TableColumn<DataSale, String> column2;

    @FXML
    private TableColumn<DataSale, String> column3;

    @FXML
    private TableColumn<DataSale, String> column4;


    public void onMainButtonClick(ActionEvent actionEvent) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("first-view.fxml"));
        Stage stage = (Stage) pOrder.getScene().getWindow();
        stage.setScene(new Scene(root));
        stage.show();
    }

    private void enterDataSale() throws SQLException, ClassNotFoundException {
        ArrayList<String> data = DBConnect.getSale();
        for (int i = 0; i < data.size(); i += 4) {
            history.add(new DataSale(data.get(i), data.get(i + 1), data.get(i + 2),data.get(i+3)));
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            enterDataSale();
            column1.setCellValueFactory(new PropertyValueFactory<DataSale, String>("date"));
            column2.setCellValueFactory(new PropertyValueFactory<DataSale, String>("form"));
            column3.setCellValueFactory(new PropertyValueFactory<DataSale, String>("client"));
            column4.setCellValueFactory(new PropertyValueFactory<DataSale, String>("auto"));
            tableview.setItems(history);
        } catch (SQLException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        try {
            ResultSet resultSet = DBConnect.executeQuerySet("select form_of_payment from form_of_payment");
            ObservableList<String> formList = FXCollections.observableArrayList();
            while (resultSet.next()){
                formList.add(resultSet.getString("form_of_payment"));
            }
            form.setItems(formList);

            resultSet = DBConnect.executeQuerySet("select FIO from client");
            ObservableList<String> clientList = FXCollections.observableArrayList();
            while (resultSet.next()){
                clientList.add(resultSet.getString("FIO"));
            }
            client.setItems(clientList);

            resultSet = DBConnect.executeQuerySet("select stamp_model_year from auto");
            ObservableList<String> autoList = FXCollections.observableArrayList();
            while (resultSet.next()){
                autoList.add(resultSet.getString("stamp_model_year"));
            }
            auto.setItems(autoList);

        } catch (SQLException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public void onDeleteButtonClick(ActionEvent actionEvent) throws SQLException, ClassNotFoundException {
        DataSale dataSale = tableview.getSelectionModel().getSelectedItem();
        String formID = "", clientID = "", autoID = "";

        ResultSet resultSet = DBConnect.executeQuerySet(
                String.format(
                        "SELECT id_form_of_payment from form_of_payment where form_of_payment = '%s'",
                        dataSale.getForm()
                ));
        if(resultSet.next()){
            formID = resultSet.getString("id_form_of_payment");
        }

        resultSet = DBConnect.executeQuerySet(
                String.format(
                        "SELECT id_client from client where FIO = '%s'",
                        dataSale.getClient()
                ));
        if(resultSet.next()){
            clientID = resultSet.getString("id_client");
        }

        resultSet = DBConnect.executeQuerySet(
                String.format(
                        "SELECT id_auto from auto where stamp_model_year = '%s'",
                        dataSale.getAuto()
                ));
        if(resultSet.next()){
            autoID = resultSet.getString("id_auto");
        }

        DBConnect.executeQuery(String.format(
                "DELETE FROM sale WHERE form_of_payment_id_form_of_payment = '%s' AND client_id_client = '%s' AND auto_id_auto = '%s';",
                formID,
                clientID,
                autoID
        ));
    }

    public void onAssButtonClick(ActionEvent actionEvent) throws SQLException, ClassNotFoundException {
        String formID = "", clientID = "", autoID = "";

        ResultSet resultSet = DBConnect.executeQuerySet(
                String.format(
                        "SELECT id_form_of_payment from form_of_payment where form_of_payment = '%s'",
                        form.getSelectionModel().getSelectedItem().toString()
                ));
        if(resultSet.next()){
            formID = resultSet.getString("id_form_of_payment");
        }

        resultSet = DBConnect.executeQuerySet(
                String.format(
                        "SELECT id_client from client where FIO = '%s'",
                        client.getSelectionModel().getSelectedItem().toString()
                ));
        if(resultSet.next()){
            clientID = resultSet.getString("id_client");
        }

        resultSet = DBConnect.executeQuerySet(
                String.format(
                        "SELECT id_auto from auto where stamp_model_year = '%s'",
                        auto.getSelectionModel().getSelectedItem().toString()
                ));
        if(resultSet.next()){
            autoID = resultSet.getString("id_auto");
        }

        String query = String.format(
                "INSERT INTO sale values(NULL, '%s', '%s', '%s', '%s')",
                date.getText(),
                formID,
                clientID,
                autoID
        );
        DBConnect.executeQuery(query);
    }
}

