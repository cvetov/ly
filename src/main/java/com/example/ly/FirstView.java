package com.example.ly;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class FirstView implements Initializable {
    public static String login;

    @FXML
    Label fio;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        try {
            fio.setText(DBConnect.getInformationClient(login).get("FIO"));
        } catch (SQLException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public void onClientButtonClick() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("clients-view.fxml"));
        Stage stage = (Stage) fio.getScene().getWindow();
        stage.setScene(new Scene(root));
        stage.show();
    }

    public void onAutoButtonClick(ActionEvent actionEvent) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("auto-view.fxml"));
        Stage stage = (Stage) fio.getScene().getWindow();
        stage.setScene(new Scene(root));
        stage.show();
    }

    public void onOrderButtonClick(ActionEvent actionEvent) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("order-view.fxml"));
        Stage stage = (Stage) fio.getScene().getWindow();
        stage.setScene(new Scene(root));
        stage.show();
    }


}
