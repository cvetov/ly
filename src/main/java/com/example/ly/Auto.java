package com.example.ly;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class Auto implements Initializable {

    @FXML
    Label pAuto;

    private ObservableList<DataAuto> history = FXCollections.observableArrayList();

    @FXML
    private TableView<DataAuto> tableview;

    @FXML
    private TableColumn<DataAuto, String> column1;

    @FXML
    private TableColumn<DataAuto, String> column2;
    @FXML
    private TableColumn<DataAuto, String> column3;


    @FXML
    private TextField marks;

    @FXML
    private TextField desc;

    @FXML
    private TextField price;




    private void enterDataAuto() throws SQLException, ClassNotFoundException {
        ArrayList<String> data = DBConnect.getDateAuto();
        for (int i = 0; i < data.size(); i += 3) {
            history.add(new DataAuto(data.get(i), data.get(i + 1), data.get(i + 2)));
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            enterDataAuto();
            column1.setCellValueFactory(new PropertyValueFactory<DataAuto, String>("stamp_model_year"));
            column2.setCellValueFactory(new PropertyValueFactory<DataAuto, String>("description_auto"));
            column3.setCellValueFactory(new PropertyValueFactory<DataAuto, String>("price"));
            tableview.setItems(history);
        } catch (SQLException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public void onMainButtonClick(ActionEvent actionEvent) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("first-view.fxml"));
        Stage stage = (Stage) pAuto.getScene().getWindow();
        stage.setScene(new Scene(root));
        stage.show();
    }


    public void onAutoButtonClick(ActionEvent actionEvent) throws SQLException, ClassNotFoundException {
        String query = String.format(
                "INSERT INTO auto values(DEFAULT, '%s', '%s', '%s')",
                marks.getText(),
                desc.getText(),
                price.getText()
        );
        DBConnect.executeQuery(query);
    }


}
