package com.example.ly;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DBConnect {



    private static String host;

    private static String port;

    private static String nameDB;


    private static String userConnect;

    private static String passwordConnect;


    public DBConnect() {
        Map<String, String> env = System.getenv();
        host = env.getOrDefault("DB_HOST", "localhost");
        port = env.getOrDefault("DB_PORT", "3306");
        nameDB = env.getOrDefault("DB_NAME", "test");
        userConnect = env.getOrDefault("DB_USER", "root");
        passwordConnect = env.getOrDefault("DB_PASS", "");
    }

    public static boolean getDBConnection(String login, String password) throws SQLException, ClassNotFoundException {



        String connectionString = "jdbc:mysql://" + host + ":"
                + port + "/" + nameDB;
        Class.forName("com.mysql.cj.jdbc.Driver");

        Connection connection = DriverManager.getConnection(connectionString, userConnect, passwordConnect);
        String query = "Select login,password from employee";
        PreparedStatement statement = connection.prepareStatement(query);
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            if ((resultSet.getString(1).equals(login)) && (resultSet.getString(2).equals(password))) {
                FirstView.login = login;
                return true;
            }
        }
        connection.close();
        return false;
    }

    public static ArrayList<String> getDate() throws SQLException, ClassNotFoundException {
        String connectionString = "jdbc:mysql://" + host + ":"
                + port + "/" + nameDB;
        Class.forName("com.mysql.cj.jdbc.Driver");
        ArrayList<String> arrayList = new ArrayList<>();
        Connection connection = DriverManager.getConnection(connectionString, userConnect, passwordConnect);
        String enter = "SELECT FIO, pasport_data, number_phone, email  from client;";
        PreparedStatement statement = connection.prepareStatement(enter);
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            arrayList.add(resultSet.getString(1));
            arrayList.add(resultSet.getString(2));
            arrayList.add(resultSet.getString(3));
            arrayList.add(resultSet.getString(4));
        }
        return arrayList;
    }

    public static ArrayList<String> getSale() throws SQLException, ClassNotFoundException {

        String connectionString = "jdbc:mysql://" + host + ":"
                + port + "/" + nameDB;
        Class.forName("com.mysql.cj.jdbc.Driver");

        ArrayList<String> arrayList = new ArrayList<>();
        Connection connection = DriverManager.getConnection(connectionString, userConnect, passwordConnect);
        String enter = "SELECT date_of_sale, form_of_payment.form_of_payment, client.FIO, auto.stamp_model_year from form_of_payment, client, auto, sale where sale.client_id_client=client.id_client and form_of_payment.id_form_of_payment=sale.form_of_payment_id_form_of_payment and auto.id_auto=sale.auto_id_auto;";
        PreparedStatement statement = connection.prepareStatement(enter);
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            arrayList.add(resultSet.getString(1));
            arrayList.add(resultSet.getString(2));
            arrayList.add(resultSet.getString(3));
            arrayList.add(resultSet.getString(4));
        }
        return arrayList;
    }

    public static void executeQuery(String query) throws SQLException, ClassNotFoundException {

        String connectionString = "jdbc:mysql://" + host + ":"
                + port + "/" + nameDB;
        Class.forName("com.mysql.cj.jdbc.Driver");

        Connection connection = DriverManager.getConnection(connectionString, userConnect, passwordConnect);
        Statement statement = connection.createStatement();
        statement.execute(query);
        statement.close();
        connection.close();
    }

    public static ResultSet executeQuerySet(String query) throws SQLException, ClassNotFoundException {

        String connectionString = "jdbc:mysql://" + host + ":"
                + port + "/" + nameDB;
        Class.forName("com.mysql.cj.jdbc.Driver");

        Connection connection = DriverManager.getConnection(connectionString, userConnect, passwordConnect);
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        return resultSet;
    }


    public static ArrayList<String> getDateAuto() throws SQLException, ClassNotFoundException {

        String connectionString = "jdbc:mysql://" + host + ":"
                + port + "/" + nameDB;
        Class.forName("com.mysql.cj.jdbc.Driver");

        ArrayList<String> arrayListAuto = new ArrayList<>();
        Connection connection = DriverManager.getConnection(connectionString, userConnect, passwordConnect);
        String enter = "select stamp_model_year, description, price from auto;";
        PreparedStatement statement = connection.prepareStatement(enter);
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            arrayListAuto.add(resultSet.getString(1));
            arrayListAuto.add(resultSet.getString(2));
            arrayListAuto.add(resultSet.getString(3));
        }
        return arrayListAuto;
    }


    public static Map<String, String> getInformationClient(String login) throws SQLException, ClassNotFoundException {

        String connectionString = "jdbc:mysql://" + host + ":"
                + port + "/" + nameDB;
        Class.forName("com.mysql.cj.jdbc.Driver");

        Connection connection = DriverManager.getConnection(connectionString, userConnect, passwordConnect);
        String query = "Select FIO from employee where login = '" + login + "'";
        PreparedStatement statement = connection.prepareStatement(query);
        ResultSet resultSet = statement.executeQuery();
        Map<String, String> returnState = new HashMap<>();
        while (resultSet.next()) {
            returnState.put("FIO", resultSet.getString(1));
        }
        connection.close();
        return returnState;
    }

}
