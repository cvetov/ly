package com.example.ly;

public class Data {
    private String FIO;
    private String pasport_data;
    private String number_phone;
    private String email;


//    private String history;

    public Data(String FIO, String pasport_data,String number_phone, String email) {
        this.FIO=FIO;
        this.pasport_data=pasport_data;
        this.number_phone=number_phone;
        this.email=email;
    }




    public String getFIO() {
        return FIO;
    }

    public void setFIO(String FIO) {
        this.FIO = FIO;
    }

    public String getEmail() {
        return email;
    }

    public String getNumber_phone() {
        return number_phone;
    }

    public String getPasport_data() {
        return pasport_data;
    }



    //    public String getHistory() {
//        return history;
//    }
//
//    public void setHistory(String history) {
//        this.history = history;
//    }
}
