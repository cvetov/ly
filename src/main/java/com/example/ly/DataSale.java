package com.example.ly;

public class DataSale {
    private String auto;
    private String client;
    private String date;
    private String form;


//    private String history;

    public DataSale(String date, String form,String client, String auto) {
        this.auto=auto;
        this.client=client;
        this.date=date;
        this.form=form;
    }


    public String getAuto() {
        return auto;
    }

    public String getClient() {
        return client;
    }

    public String getDate() {
        return date;
    }

    public String getForm() {
        return form;
    }
}
