package com.example.ly;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class CaptchaController implements Initializable {

    String textCaptcha;
    @FXML
    Canvas canva;

    @FXML
    TextField field;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        CaptchaGenerator generator = new CaptchaGenerator(canva);
        textCaptcha = generator.generate(4);
    }

    public void onSendClick() {
        if(textCaptcha.equals(field.getText())){
            Stage stage = (Stage) field.getScene().getWindow();
            stage.close();
            HelloController.coltry = 0;
        }
    }
}