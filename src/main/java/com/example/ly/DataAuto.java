package com.example.ly;

public class DataAuto {

    private String stamp_model_year;

    private String description_auto;

    private String price;

//    private String history;

    public DataAuto(String stamp_model_year, String description_auto, String price) {
        this.stamp_model_year=stamp_model_year;
        this.description_auto=description_auto;
        this.price=price;
    }


    public String getDescription_auto() {
        return description_auto;
    }

    public String getPrice() {
        return price;
    }

    public String getStamp_model_year() {
        return stamp_model_year;
    }
}
