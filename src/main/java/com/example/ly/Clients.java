package com.example.ly;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;


public class Clients implements Initializable {
    @FXML
    Label pClients;

    private ObservableList<Data> history = FXCollections.observableArrayList();

    @FXML
    private TableView<Data> tableview;

    @FXML
    private TableColumn<Data, String> column1;

    @FXML
    private TableColumn<Data, String> column2;
    @FXML
    private TableColumn<Data, String> column3;
    @FXML
    private TableColumn<Data, String> column4;

    @FXML
    private TextField fio;

    @FXML
    private TextField mail;

    @FXML
    private TextField pasport;

    @FXML
    private TextField phone;

    public void onMainButtonClick(ActionEvent actionEvent) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("first-view.fxml"));
        Stage stage = (Stage) pClients.getScene().getWindow();
        stage.setScene(new Scene(root));
        stage.show();
    }


    private void enterData() throws SQLException, ClassNotFoundException {
        ArrayList<String> data = DBConnect.getDate();
        for (int i = 0; i < data.size(); i += 4) {
            history.add(new Data(data.get(i), data.get(i + 1), data.get(i + 2), data.get(i + 3)));
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            enterData();
            column1.setCellValueFactory(new PropertyValueFactory<Data, String>("FIO"));
            column2.setCellValueFactory(new PropertyValueFactory<Data, String>("pasport_data"));
            column3.setCellValueFactory(new PropertyValueFactory<Data, String>("number_phone"));
            column4.setCellValueFactory(new PropertyValueFactory<Data, String>("email"));
            tableview.setItems(history);
        } catch (SQLException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public void onAddButtonClick(ActionEvent actionEvent) throws SQLException, ClassNotFoundException {
        String query = String.format(
                "INSERT INTO client values(DEFAULT, '%s', '%s', '%s', '%s')",
                fio.getText(),
                pasport.getText(),
                phone.getText(),
                mail.getText()
        );
        DBConnect.executeQuery(query);
    }
}
